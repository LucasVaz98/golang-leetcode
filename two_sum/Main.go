package main

import "fmt"

func twoSum(nums []int, target int) []int {
	_index := []int{}

	for index1, num1 := range nums {
		for index2, num2 := range nums {
			if index2 > index1 && num1+num2 == target {
				_index = append([]int{index1, index2}, _index...)
			}
		}
	}

	return _index
}

func main() {
	fmt.Printf("%v\n", twoSum([]int{2, 7, 11, 15}, 9))
	fmt.Printf("%v\n", twoSum([]int{3, 2, 4}, 6))
}
