package main

import (
	"fmt"
	"os"
	"strconv"
)

func isPalindrome(x int) bool {
	var reversedNum int
	tmp := x

	for tmp > 0 {
		reversedNum = reversedNum*10 + tmp%10
		tmp = tmp / 10
	}

	return x == reversedNum
}

func main() {
	arg := os.Args[1]
	input, _ := strconv.ParseInt(arg, 10, 32)
	fmt.Println(isPalindrome(int(input)))
}
